let express = require('express'),
    bodyParser = require('body-parser'),
    _ = require('lodash'),
    passport = require('passport'),
    passportJWT = require('passport-jwt'),
    jwt = require('jsonwebtoken');

let ExtractJwt = passportJWT.ExtractJwt;
let JwtStrategy = passportJWT.Strategy;


//users
let users = [
    {
        id: 1,
        name: "admin",
        password: "admin"
    },
    {
        id: 2,
        name: "test",
        password: "test"
    }
]


//authentication

const jwtOptions = {}

jwtOptions.jwtFromRequest =  ExtractJwt.fromAuthHeaderAsBearerToken()
jwtOptions.secretOrKey = 'issabigtingmehn'

let strategy = new JwtStrategy(jwtOptions, (jwt_payload, next)=>{
    console.log('payload received', jwt_payload);
    var user = users[_.findIndex(users, {id : jwt_payload.id})];
    if (user) {
        next(null, user)
    } else {
        next(null, false)
    } 
})

passport.use(strategy)




let app = express();
app.use(passport.initialize())
app.use(express.static('public'))

app.use(bodyParser.urlencoded({extended: true}))
app.use(bodyParser.json())


app.get('/', (req, res)=>{
    //res.json({message: 'success i can be reached'})
    res.sendFile(__dirname + '/public/index.html');
})


app.post('/login', (req, res)=>{
    
    if (req.body.name && req.body.password) {
        var name = req.body.name;
        var password = req.body.password;
    }

    //revert to database call
    let user = users[_.findIndex(users, {name: name})]

    if (!user) {
        res.status(401).json({message: 'User not found'})
    }
    

    if (user.password === req.body.password) {
        //user will now be identified strictly via the id
        let payload = {id: user.id};
        let token = jwt.sign(payload, jwtOptions.secretOrKey);

        res.json({message: 'success, you were logged in', token: token})
    } else {
        res.status(401).json({message: "Sorry password mismatch"})
    }
})

app.get('/secret', passport.authenticate('jwt', {session: false}), (req, res)=>{
    res.json("Congrats if you can see this")
})




// app.get("/secret", passport.authenticate('jwt', { session: false }), function(req, res){
//     res.json("Success! You can not see this without a token");
//   });




// app.post("/login", function(req, res) {
//     if(req.body.name && req.body.password){
//       var name = req.body.name;
//       var password = req.body.password;
//     }
//     // usually this would be a database call:
//     var user = users[_.findIndex(users, {name: name})];
//     if( ! user ){
//       res.status(401).json({message:"no such user found"});
//     }
  
//     if(user.password === req.body.password) {
//       // from now on we'll identify the user by the id and the id is the only personalized value that goes into our token
//       var payload = {id: user.id};
//       var token = jwt.sign(payload, jwtOptions.secretOrKey);
//       res.json({message: "ok", token: token});
//     } else {
//       res.status(401).json({message:"passwords did not match"});
//     }
//   });


app.listen(3000,()=>{
    console.log('Server up and running on port 3000')
})